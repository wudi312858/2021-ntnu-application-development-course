package AD2021.MultiThreadATM;

//This is to test thread communication
//Requirements: two thread print integers alternately
class NumberPrint implements Runnable {
    private int number = 1;

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                //revoke the thread waiting...
                //In this example, there are two threads,
                // so we use notify() instead of notifyAll()
                // Try to build 3 threads, what can you get?
                //notifyAll();
                notify();
                if (number <= 100) {
                    System.out.println(Thread.currentThread().getName() + ": " + number);
                    number++;
                    //block this thread in order to alternate.
                    //wait() would release monitor.
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else break;
            }
        }

    }
}

public class ATMTicketsTest6 {
    public static void main(String[] args) {
        NumberPrint np = new NumberPrint();
        Thread t1 = new Thread(np);
        Thread t2 = new Thread(np);



        t1.setName("Thread 1");
        t2.setName("Thread 2");


        t1.start();
        t2.start();


    }


}
