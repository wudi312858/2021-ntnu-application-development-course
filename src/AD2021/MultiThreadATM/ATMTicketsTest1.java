/*
To test ATM by extends Thread
 */
package AD2021.MultiThreadATM;

class ATMThread1 extends Thread {
    private static int ticketNum = 100;
    static Object obj = new Object();

    @Override
    public void run() {
        while (true) { //we cannot define how many tickets sold by one machine.
            synchronized (ATMThread1.class) {
                if (ticketNum > 0) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + " sold with ticket number: " + ticketNum);
                    ticketNum--;

                } else break;
            }
        }
    }
}

public class ATMTicketsTest1 {
    public static void main(String[] args) {

        ATMThread1 atmThread1 = new ATMThread1();
        atmThread1.setName("Machine 1");
        ATMThread1 atmThread2 = new ATMThread1();
        atmThread2.setName("Machine 2");
        ATMThread1 atmThread3 = new ATMThread1();
        atmThread3.setName("Machine 3");


        atmThread1.start();
        atmThread2.start();
        atmThread3.start();
    }


}
