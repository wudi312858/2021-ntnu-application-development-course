/*
This is to test ticket example using synchronization
method with extends Thread.
 */
package AD2021.MultiThreadATM;

class ATMThread3 extends Thread {
    private static int ticketNum = 100;
    static Object obj = new Object();

    @Override
    public void run() {
        while (true) { //we cannot define how many tickets sold by one machine.
            show();
        }
    }
//This is a wrong claim
    //private synchronized void show() {

    //This is the correct way to claim synchronized method.
    private static synchronized void show() {

        //In this way, since we remove eles break;
        // so you have to stop the execution manually.
        if (ticketNum > 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " sold with ticket number: " + ticketNum);
            ticketNum--;

        }
    }
}

public class ATMTicketsTest3 {
    public static void main(String[] args) {
        ATMThread3 atmThread1 = new ATMThread3();
        atmThread1.setName("Machine 1");
        ATMThread3 atmThread2 = new ATMThread3();
        atmThread2.setName("Machine 2");
        ATMThread3 atmThread3 = new ATMThread3();
        atmThread3.setName("Machine 3");


        atmThread1.start();
        atmThread2.start();
        atmThread3.start();
    }
}
