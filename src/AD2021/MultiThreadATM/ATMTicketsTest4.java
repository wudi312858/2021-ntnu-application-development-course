package AD2021.MultiThreadATM;

/*ATM test using implements Runnable with synchronized method*/


class ATMThread4 implements Runnable {
    private static int ticketNum = 100;
    Dog dog = new Dog();
    Object obj = new Object();

    @Override
    public void run() {
        //wrong expression here.
//        Object obj = new Object();
        while (true) { //we cannot define how many tickets sold by one machine.
            show();
        }
    }

    private synchronized void show() {
        //include all the code that operating the shared parameter.
        if (ticketNum > 0) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " sold with ticket number: " + ticketNum);
            ticketNum--;

        }
    }
}

public class ATMTicketsTest4 {

    public static void main(String[] args) {
        ATMThread4 atmThread = new ATMThread4();
        Thread t1 = new Thread(atmThread);
        Thread t2 = new Thread(atmThread);
        Thread t3 = new Thread(atmThread);

        t1.setName("Machine 1");
        t2.setName("Machine 2");
        t3.setName("Machine 3");

        t1.start();
        t2.start();
        t3.start();

    }


}

