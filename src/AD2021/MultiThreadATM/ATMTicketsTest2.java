/*ATM test using implements Runnable*/
/*
Problem: Duplicates, wrong tickets: thread security issues.
Reason: Thread is not finished, another thread is also coming to execute.
How to solve: synchronization mechanism.

1. The code that operates shared parameter is the one need to be synchronized
2. Shared parameter: multiple threads are operating one parameter. (ticketNum)
3. Synchronization monitor (lock):
    Any object can take as a monitor.
    Just one requirement: all threads must use the same monitor.

Synchronization solved thread security problem.However, it slows down the execution.

 */
package AD2021.MultiThreadATM;

class ATMThread2 implements Runnable {
    private static int ticketNum = 100;
    Dog dog = new Dog();
    Object obj = new Object();
    @Override
    public void run() {
        //wrong expression here.
//        Object obj = new Object();
        while (true) { //we cannot define how many tickets sold by one machine.
            synchronized (this) {
                //include all the code that operating the shared parameter.
                if (ticketNum > 0) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + " sold with ticket number: " + ticketNum);
                    ticketNum--;

                } else break;
            }


        }
    }
}

public class ATMTicketsTest2 {

    public static void main(String[] args) {
        ATMThread2 atmThread = new ATMThread2();
        Thread t1 = new Thread(atmThread);
        Thread t2 = new Thread(atmThread);
        Thread t3 = new Thread(atmThread);

        t1.setName("Machine 1");
        t2.setName("Machine 2");
        t3.setName("Machine 3");

        t1.start();
        t2.start();
        t3.start();

    }


}
class Dog {

}