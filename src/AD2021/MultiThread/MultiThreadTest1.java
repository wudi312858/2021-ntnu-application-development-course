package AD2021.MultiThread;
/*
This is to create and test multi-threading using Thread class.

Step 1: create a subclass of Thread.
Step 2: override run() method.
Step 3: create an instance of the subclass.
Step 4: call the start() method of that instance.

Example: write a thread to find all the even numbers below 100.
 */

import java.util.ArrayList;

//Step 1: create a subclass of Thread.
class EvenNumberThread1 extends Thread {
    //Step 2: override run() method.

    @Override
    public void run() {
        // write a thread to find all the even numbers below 100.
        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                System.out.println(Thread.currentThread().getName() + " " + i);
            }
        }

    }
}

public class MultiThreadTest1 {
    public static void main(String[] args) {
        //  Step 3: create an instance of the subclass.
        EvenNumberThread1 evenNumberThread1 = new EvenNumberThread1();
        //  Step 4: call the start() method of that instance.
        evenNumberThread1.start();
        //evenNumberThread1.run();

        //wrong.
        //evenNumberThread1.start();

        EvenNumberThread1 evenNumberThread2 = new EvenNumberThread1();
        evenNumberThread2.start();

        // System.out.println("Hello World!");
        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                System.out.println(Thread.currentThread().getName() + " " + i);
            }
        }



    }
}
