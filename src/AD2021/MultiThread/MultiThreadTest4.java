package AD2021.MultiThread;

/*
This is to test different methods of Thread.
Method 1. start(): initiates thread and executes run() method of the instance.
Method 2. run(): need to be override. all the instructions needs to be written here.
Method 3. currentThread(): static method, return the current thread.
Method 4. getName(): get the name of the current thread.
Method 5: setName(): set the name of the current thread.
Method 6: yield(): release the current execution of processor to other thread.
Method 7: join(): in A thread to call thread B.join, then A thread is blocked until B is finished.
Method 8: stop(): deprecated. When this method is used, thread is forced to stop.
Method 9: sleep(long millitime): block the thread for specified time then wait for the CPU for further distribution.
Method 10: isAlive(): judge the status of the thread.

Priority
1. Constants: (these constants are defined in Thread class)
MAX_PRIORITY: 10
MIN_PRIORITY: 1
NORM_PRIORITY: 5 //default
2. Get and Set priority.

 */
class EvenNumberThread4 extends Thread {

    //Method 2.
    @Override
    public void run() {
        setPriority(Thread.MAX_PRIORITY);

        System.out.println("Even Priority: " + Thread.currentThread().getPriority());

        for (int i = 0; i < 50; i++) {

            if (i % 2 == 0) {
                //Method 9.
//                try {
//                    sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                //Method 3 & 4.
                System.out.println(Thread.currentThread().getName() + ": " + i);
            }


        }
    }
}

public class MultiThreadTest4 {
    public static void main(String[] args) {
        EvenNumberThread4 evenNumberThread4 = new EvenNumberThread4();
        //Method 5.
        evenNumberThread4.setName("Even - ");
        //Method 1.
        evenNumberThread4.start();

//        evenNumberThread4.setName("Server - Even123 - ");
        //Method 5.
        Thread.currentThread().setName("Main Thread - ");

        //Priority set and get.
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
        System.out.println("Main Priority: " + Thread.currentThread().getPriority());
        for (int i = 0; i < 50; i++) {

            if (i % 2 == 0) {
                //Method 3.
                System.out.println(Thread.currentThread().getName() + ": " + i);
            }
            //Method 6.
//            if (i % 10 == 0) Thread.yield();
            //Method 7.
//            if (i == 24){
//                try {
//                    evenNumberThread4.join();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }


        }
        //Method 10.
        System.out.println(evenNumberThread4.isAlive());
    }
}
