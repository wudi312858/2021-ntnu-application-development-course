package AD2021.MultiThread;
/*
This is to test the 2nd way to realize thread using implement Runnable interface.
Step 1: create a class that implements Runnable.
Step 2: override run() method.
Step 3: create an instance for the class.
Step 4: put this instance as a parameter to the constructor of Thread class.
Step 5: use Thread instance to call start() method.
 */

//Step 1: create a class that implements Runnable.
class EvenNumberThread3 implements Runnable {
    //Step 2: override run() method.
    @Override
    public void run() {
        for (int i = 0; i < 50; i++) {
            if (i % 2 == 0) {
                System.out.println(Thread.currentThread().getName() + ": " + i);
            }

        }
    }
}

public class MultiThreadTest3 {
    public static void main(String[] args) {

        //Step 3: create an instance for the class.
        EvenNumberThread3 evenNumberThread3 = new EvenNumberThread3();
        //Step 4: put this instance as a parameter to the constructor of Thread class.
        Thread t1 = new Thread(evenNumberThread3);
        //Step 5: use Thread instance to call start() method.
        //Question: t1.run() will call evenNumberThread3 run() method?
        t1.start();

        //Start another thread for even numbers.
        Thread t2 = new Thread(evenNumberThread3);
        t2.start();

    }

}
